<%@ page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%request.setCharacterEncoding("utf-8");%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8”>
        <title></title>
    <style type="text/css">
        .smart-green {
            margin-left: auto;
            margin-right: auto;
            max-width: 67%;
            background: #F8F8F8;
            padding: 30px 30px 20px 30px;
            font: 12px Arial, Helvetica, sans-serif;
            color: #666;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
        }

        .smart-green h1 {
            font: 24px "Trebuchet MS", Arial, Helvetica, sans-serif;
            padding: 20px 0px 20px 40px;
            display: block;
            margin: -30px -30px 10px -30px;
            color: #FFF;
            background: #9DC45F;
            text-shadow: 1px 1px 1px #949494;
            border-radius: 5px 5px 0px 0px;
            -webkit-border-radius: 5px 5px 0px 0px;
            -moz-border-radius: 5px 5px 0px 0px;
            border-bottom: 1px solid #89AF4C;

        }

        .smart-green h1 > span {
            display: block;
            font-size: 11px;
            color: #FFF;
        }

        .smart-green label {
            display: block;
            margin: 0px 0px 5px;
        }

        .smart-green label > span {
            float: left;
            margin-top: 10px;
            color: #5E5E5E;
        }

        .smart-green input[type="text"], .smart-green input[type="email"], .smart-green textarea, .smart-green select {
            color: #555;
            height: 30px;
            line-height: 15px;
            width: 100%;
            padding: 0px 0px 0px 10px;
            margin-top: 2px;
            border: 1px solid #E5E5E5;
            background: #FBFBFB;
            outline: 0;
            -webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
            box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
            font: normal 14px/14px Arial, Helvetica, sans-serif;
        }

        .smart-green textarea {
            height: 100px;
            padding-top: 10px;
        }

        .smart-green select {
            background: url('down-arrow.png') no-repeat right, -moz-linear-gradient(top, #FBFBFB 0%, #E9E9E9 100%);
            background: url('down-arrow.png') no-repeat right, -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FBFBFB), color-stop(100%, #E9E9E9));
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 0.01px;
            text-overflow: '';
            width: 100%;
            height: 30px;
        }

        .smart-green .button {
            background-color: #9DC45F;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-border-radius: 5px;
            border: none;
            padding: 10px 25px 10px 25px;
            color: #FFF;
            text-shadow: 1px 1px 1px #949494;
        }

        .smart-green .button:hover {
            background-color: #80A24A;
        }
    </style>

    <script type="text/javascript" charset="utf-8" src=" UEditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src=" UEditor/ueditor.all.min.js"></script>
    <script type="text/javascript" charset="utf-8" src=" UEditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<form action="/AddNewsServlet" method="post" class="smart-green">
    <h1>新闻管理表单
        <span>请输入完整的新闻内容</span>
    </h1>
    <label>
        <span>新闻 ID :</span>
        <input id="newsID" type="text" name="newsID" placeholder="newsID"/>
        <%--        <input id="newsID" type="text" name="newsID" placeholder="newsID" value="${new.newID}"/>--%>
    </label>
    <label>
        <span>新闻 标题 :</span>
        <%--        <input id="newsTitle" type="text" name="newsTitle" placeholder="newsTitle" value="${new.newsTitle}"/>--%>
        <input id="newsTitle" type="text" name="newsTitle" placeholder="newsTitle"/>
    </label>

    <label>
        <span>新闻 作者 :</span>
        <%--        <input id="newsAuthor" type="text" name="newsAuthor" placeholder="newsAuthor" value="${new.newsAuthor}"/>--%>
        <input id="newsAuthor" type="text" name="newsAuthor" placeholder="newsAuthor"/>
    </label>

    <label>
        <span>新闻 时间 :</span>
        <input id="newsDate" type="text" name="newsDate" placeholder="newsDate"/>
        <%--        <input id="newsDate" type="text" name="newsDate" placeholder="newsDate" value="${new.newsDate}"/>--%>
    </label>

    <label>
        <span>新闻 内容 :</span>
        <%--        <script id="editor" type="text/plain" style="width:1024px;height:500px;"></script>--%>
        <%--        <textarea id="newsContent" name="newsContent" placeholder="newsContent" value="${new.newsContent}"></textarea>--%>
        <%--        <textarea id="newsContent" name="newsContent" placeholder="newsContent"></textarea>--%>
        <script id="editor" type="text/plain" style="width:100%;height:300px;padding-top: 30px;"></script>
    </label>

    <label>
        <span>&nbsp;</span>
        <input type="submit" class="button" value="更新"/>
    </label>
</form>

<script type="text/javascript">

    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');

    function getContent() {
        var arr = [];
        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        alert(arr.join("\n"));
    }

</script>
</body>
</html>

<%@ page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%request.setCharacterEncoding("utf-8");%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8”>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>西南石油大学-计算机科学学院</title>
    <link rel="stylesheet" href="/css/home.css">
</head>
<body>
<!-- 顶部搜索导航栏开始 -->
<div class="search">
    <div class="searchlogo">
        <span>请输入关键字搜索</span>
        <img src="image/top_hunt.png" alt="">
    </div>
</div>
<!-- 顶部搜索导航栏结束 -->
<!-- 导航栏开始 -->
<div class="nav clearfix">
    <ul class="ul1">
        <li><a href="#">网站首页</a></li>
        <li><a href="#">学院概况</a>
            <ul class="ull2">
                <li><a href="#">学院简介</a></li>
                <li><a href="#">学院领导</a></li>
                <li><a href="#">组织机构</a></li>
            </ul>
        </li>
        <li><a href="#">本科生教育</a>
            <ul class="ul3">
                <li><a href="#">专业设置</a></li>
                <li><a href="#">对外人才合作培养项目</a></li>
                <li><a href="#">考试信息</a></li>
                <li><a href="#">选课重修信息</a></li>
                <li><a href="#">通知公告</a></li>
                <li><a href="#">资料下载</a></li>
                <li><a href="#">公选课教师视频</a></li>
                <li><a href="#">教学项目</a></li>
                <li><a href="#">教学获奖</a></li>
                <li><a href="#">教学大纲</a></li>
            </ul>
        </li>
        <li><a href="#">研究生教育</a>
            <ul class="ul3">
                <li><a href="#">招生简章</a></li>
                <li><a href="#">计算机科学与技术一级学科</a></li>
                <li><a href="#">软件工程一级学科</a></li>
                <li><a href="#">网络空间安全一级学科</a></li>
                <li><a href="#">研究生导师</a></li>
                <li><a href="#">研究生奖助体系</a></li>
                <li><a href="#">通知公告</a></li>
                <li><a href="#">资料下载</a></li>
            </ul>
        </li>
        <li><a href="#">科学研究</a>
            <ul class="ull2">
                <li><a href="#">科研团队</a></li>
                <li><a href="#">科研平台</a></li>
                <li><a href="#">科研成果</a></li>
            </ul>
        </li>
        <li><a href="#">学生工作</a>
            <ul class="ul3">
                <li><a href="#">工作动态</a></li>
                <li><a href="#">通知公告</a></li>
                <li><a href="#">课外创新实践</a></li>
                <li><a href="#">毕业生就业</a></li>
                <li><a href="#">心灵之窗</a></li>
                <li><a href="#">青春风采</a></li>
                <li><a href="#">资料下载</a></li>
            </ul>
        </li>
        <li><a href="#">招生工作</a>
            <ul class="ul3">
                <li><a href="#">学院介绍</a></li>
                <li><a href="#">毕业生就业去向</a></li>
                <li><a href="#">优秀毕业生简介</a></li>
                <li><a href="#">教师获奖</a></li>
                <li><a href="#">学生获奖</a></li>
                <li><a href="#">精英校友介绍</a></li>
                <li><a href="#">招生工作宣传报道</a></li>
            </ul>
        </li>
        <li><a href="#">实验中心</a>
            <ul class="ull2">
                <li><a href="#">中心简介</a></li>
                <li><a href="#">实验分室</a></li>
                <li><a href="#">规章制度</a></li>
                <li><a href="#">资料下载</a></li>
                <li><a href="#">开放预约</a></li>
            </ul>
        </li>
        <li><a href="#">党建之窗</a>
            <ul class="ull2">
                <li><a href="#">党建动态</a></li>
                <li><a href="#">学习园地</a></li>
                <li><a href="#">党务政务</a></li>
                <li><a href="#">资料下载</a></li>
            </ul>
        </li>
        <li><a href="#" class="emm">抗击疫情</a></li>
    </ul>
    <div></div>
</div>
<!-- 总导航栏结束 -->
<div class="img">
    <img src="image/colonavirus.jpg" alt="" width="970px">
</div>
<!-- 新闻导航部分1开始 -->
<div class="newsleft">
    <button>图片新闻</button>
    <img src="image/more.png" alt="">
</div>
<div class="newsright">
    <button>学术交流</button>
    <img src="image/more.png" alt="">
</div>
<!-- 新闻导航部分1结束 -->
<!-- 新闻部分1开始 -->
<div class="news clearfix">
    <img src="image/picture.jpg" alt="">
    <div class="ul">
        <ul>

            <c:forEach var="n" items="${News}">
                <li><a href="/ViewNewsServlet?newid=${n.newsId}"><p>${n.title}</p> <span> ${n.newsDate}</span></a></li>
            </c:forEach>
<%--            <li><a href="#"><p>YOCSEF成都举办“‘成’心为水水水水水水水水水水水水水水水水水水水</p> <span> [06-16]</span></a></li>--%>
<%--            <li><a href="#">思政教育“不下线”,计科院成立湖...&nbsp;&nbsp;&nbsp;&nbsp;<span>[03-24]</span></a></li>--%>
<%--            <li><a href="#">我院2020年硕士研究生招生考试工...&nbsp;&nbsp;&nbsp;<span> [05-27]</span></a></li>--%>
<%--            <li><a href="#">面向全球！我院1门全英文在线开放...&nbsp;&nbsp;<span> [05-25]</span></a></li>--%>
<%--            <li><a href="#">2020年中国计算机设计大赛省赛选...&nbsp;&nbsp;&nbsp;<span> [04-20]</span></a></li>--%>
<%--            <li><a href="#">学院党委中心组围绕疫情防控等开展...&nbsp;&nbsp;<span>[04-15]</span></a></li>--%>
        </ul>
    </div>

    <div class="ul2">
        <ul>
            <li><a href="#">2020年国家自然科学基金（计算机类）选...</a></li>
            <li><a href="#">基于彩色多普勒及心室壁运动信息的左心...</a></li>
            <li><a href="#">人类流行病传播的建模和控制研究</a></li>
            <li><a href="#">从工人到工匠--我的数据库从业之路</a></li>
            <li><a href="#">对抗性恶意代码动态分析方法研究</a></li>
            <li><a href="#">Amazon Web Services 云计算</a></li>
        </ul>
    </div>
</div>
<!-- 新闻部分1结束 -->

<!-- 新闻导航部分2开始 -->
<div class="yyy">
    <div class="newsleft2">
        <button>新闻速递</button>
        <img src="image/more.png" alt="">
    </div>
    <div class="newsright2">
        <button>党建动态</button>
        <img src="image/more.png" alt="">
    </div>
    <!-- 新闻导航部分2结束 -->
    <!-- 新闻部分2开始 -->
    <div class="news2">
        <div class="key">
            <div class="content">
                <h2>&nbsp;&nbsp;CCF YOCSEF成都举办“‘成’心为你，‘职’等你来” 公益活动</h2>
                <p>
                    &nbsp;&nbsp;为进一步做好省内各高校应届毕业生就业工作，帮助高校毕业生拓宽就业信息渠道，帮助企业引进优秀人才和各类人力资源，CCF
                    YOCSEF成都积极整合高校和企业需求，借助互联网技术，携手各大企业与…<a href="#">[详细信息]</a>
                </p>
            </div>
            <div class="hr clearfix"></div>
            <div class="ull">
                <ul>
                    <li><a href="#">学院党委召开全体党员大会集体学习传达全国“两会”精神<span> 06/22</span></a></li>
                    <li><a href="#">学院举行2020届毕业生党员教育大会<span>06/22</span></a></li>
                    <li><a href="#">我院2020年硕士研究生招生考试工作圆满结束<span>05/27</span></a></li>
                    <li><a href="#">面向全球！我院1门全英文在线开放课程上线爱课程国际平台<span> 05/25</span></a></li>
                    <li><a href="#">2020年中国计算机设计大赛省赛选拔顺利完成<span>04/20</span></a></li>
                    <li><a href="#">学院党委中心组围绕疫情防控等开展专题学习研讨会<span>04/15</span></a></li>
                </ul>
            </div>
        </div>

        <div class="ul2">
            <ul>
                <li><a href="#">学院党委召开全体党员大会集体学习传达...</a></li>
                <li><a href="#">学院党委中心组围绕疫情防控等开展专题...</a></li>
                <li><a href="#">学院举行2020届毕业生党员教育大会</a></li>
                <li><a href="#">计科院召开“学生公寓党员工作站”工作...</a></li>
                <li><a href="#">【主题教育】学院召开党员领导干部“不...</a></li>
                <li><a href="#">【主题教育】计科院学生第一党支部开展...</a></li>
                <li><a href="#">【主题教育】计科院软件工程教研室党支...</a></li>
                <li><a href="#">【主题教育】学院党委举行“守初心担使...</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 新闻部分2结束 -->
<!-- 新闻部分3开始 -->
<div class="there">
    <div class="newsleft2">
        <button>通知公告</button>
        <img src="image/more.png" alt="">
    </div>
    <div class="newsright2">
        <button>专题列表</button>
        <img src="image/more.png" alt="">
    </div>

    <div class="left">
        <div class="ull">
            <ul>
                <li><a href="#">2020年计算机科学学院实验技术人员招聘结果公示</a></li>
                <li><a href="#">干部任前公示</a></li>
                <li><a href="#">考察对象公示</a></li>
                <li><a href="#">关于给予计算机科学学院软件工程专业2017级学生MD JOBAYER AKANDA退学处理决定的公告</a></li>
                <li><a href="#">计算机科学学院2020年春季学期期中教学检查工作总结</a></li>
                <li><a href="#">关于中国高校计算机大赛——2020华为云大数据挑战赛 西南石油大学校内竞赛组织事宜通知</a></li>
                <li><a href="#">关于2020年度吴文俊人工智能科学技术奖提名工作的通知</a></li>
                <li><a href="#">计算机科学学院2020年教师课堂教学质量考核实施方案公示</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="ul2">
            <ul>
                <li><a href="#">学院党委召开全体党员大会集体学习传达...</a></li>
                <li><a href="#">学院党委中心组围绕疫情防控等开展专题...</a></li>
                <li><a href="#">学院举行2020届毕业生党员教育大会</a></li>
                <li><a href="#">计科院召开“学生公寓党员工作站”工作...</a></li>
                <li><a href="#">【主题教育】学院召开党员领导干部“不...</a></li>
                <li><a href="#">【主题教育】计科院学生第一党支部开展...</a></li>
                <li><a href="#">【主题教育】计科院软件工程教研室党支...</a></li>
                <li><a href="#">【主题教育】学院党委举行“守初心担使...</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 新闻部分3结束 -->
<!-- 底部开始 -->
<div class="footer">
    Copyright© 2018 All Rights Reserved. 西南石油大学计算机科学学院
</div>
<!-- 底部结束 -->

</body>
</html>

package DAL;
import java.sql.*;

public class SqlHelper {

    public static Connection getConnection() {
        Connection connection = null;
        try {

            String url = "jdbc:mysql://localhost:3306/users?useUnicode=true&characterEncoding=utf-8&" +
                    "useSSL=false&serverTimezone=Hongkong";

            String username = "root";
            String password = "123";

            //1.加载驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.连接数据库,代表数据库
            connection = DriverManager.getConnection(url, username, password);


            System.out.println("数据库连接成功！");

            return connection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("创建连接失败！");
        return null;
    }
    //select
    public static ResultSet executeQuery(String SQL)
    {
        try
        {
            System.out.println("Querying!!!!!!!");

            Connection conn=getConnection();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(SQL);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("查询失败！");
            return null;
        }
    }

    //insert update delete
    public static boolean executeUpdate(String SQL)
    {
        try
        {
            Connection conn=getConnection();
            Statement stmt=conn.createStatement();
            int rs=stmt.executeUpdate(SQL);
            if (rs>0)
                return true;
            else
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("更新失败！");
            return false;
        }
    }
}


package Controller;

import Entity.News;
import Service.NewsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = "/SaveNewsServlet")
public class SaveNewsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        News news = new News();

        news.setNewsId(request.getParameter("newsID"));
        news.setAuthor(request.getParameter("newsAuthor"));
        news.setTitle(request.getParameter("newsTitle"));
        news.setNewsDate(request.getParameter("newsDate"));
        news.setNewsContent(request.getParameter("editorValue"));

        NewsService service = new NewsService();
        service.updateNews(news);
        request.getRequestDispatcher("ShowNewsListServlet").forward(request, response);
    }
}

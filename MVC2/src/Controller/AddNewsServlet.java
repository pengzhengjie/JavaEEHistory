package Controller;

import Entity.News;
import Service.NewsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/AddNewsServlet")
public class AddNewsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        News news = new News();

        news.setNewsId(request.getParameter("newsID"));
        news.setAuthor(request.getParameter("newsAuthor"));
        news.setTitle(request.getParameter("newsTitle"));
        news.setNewsDate(request.getParameter("newsDate"));
        news.setNewsContent(request.getParameter("editorValue"));

        NewsService service = new NewsService();
        service.addNews(news);

        request.getRequestDispatcher("ShowNewsListServlet").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        request.getRequestDispatcher("AddNews.jsp").forward(request, response);
    }
}

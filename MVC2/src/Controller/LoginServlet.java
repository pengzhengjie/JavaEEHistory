package Controller;

import Entity.UserInfo;
import Service.UsersService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            isLoginSuccess(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void isLoginSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        //获取form中的数据
        UserInfo user = new UserInfo();
        user.setuName(request.getParameter("username"));
        user.setuPwd(request.getParameter("password"));

        System.out.println(user.getuName() + ": " + user.getuPwd() +"!!!!!!!!!!!!!!!!!!!!!!!!");

        //验证用户名密码
        UsersService usersService = new UsersService();

        //确认数据库是否存在
        boolean isHasUser = usersService.QueryUsers(user);

        //存在则将结果存到Cookie
        if (isHasUser) {
            int saveyTime = 60 * 60 * 24 * 30;  //保存的时间

            //保存账号
            Cookie cookieUserName = new Cookie("username", user.getuName());
            cookieUserName.setMaxAge(saveyTime);
            response.addCookie(cookieUserName);

            //保存密码
            Cookie cookieUserPwd = new Cookie("password", user.getuPwd());
            cookieUserName.setMaxAge(saveyTime);
            response.addCookie(cookieUserPwd);

            //重定向值index.html界面
            response.sendRedirect("page/index.html");
        } else {

            //重定向值login.html界面
            response.sendRedirect("login.html");
        }
    }
}

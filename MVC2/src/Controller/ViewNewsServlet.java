package Controller;

import Entity.News;
import Service.NewsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/ViewNewsServlet")
public class ViewNewsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        String newsID = request.getParameter("newid");
        System.out.println(newsID);

        News news = null;
        NewsService service = new NewsService();
        try {
            news = service.GetNews(newsID);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println(news.getNewsId());
        System.out.println(news.getNewsContent());
        System.out.println(news.getNewsDate());
        System.out.println(news.getAuthor());

        request.setAttribute("news", news);
        //转发到content.jsp
        request.getRequestDispatcher("NewsContent.jsp").forward(request, response);
    }
}

package Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/UEditorServlet")
public class UEditorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        //拿到表单的内容
        String newsID = request.getParameter("newsID");
        String newsTitle = request.getParameter("newsTitle");
        String newsAuthor = request.getParameter("newsAuthor");
        String newsDate = request.getParameter("newsDate");
        //拿到编辑器的内容
        String content = request.getParameter("editorValue");

        //如果不为空
        if (content != null) {
            //将内容设置进属性
            request.setAttribute("newsID", newsID);
            request.setAttribute("newsTitle", newsTitle);
            request.setAttribute("newsAuthor", newsAuthor);
            request.setAttribute("newsDate", newsDate);

            request.setAttribute("content", content);
            //转发到content.jsp
            request.getRequestDispatcher("NewsContent.jsp").forward(request, response);
        } else {
            response.getWriter().append("内容为空!");
        }
    }
}

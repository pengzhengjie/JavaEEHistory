package Entity;

import java.sql.Date;

public class News {

    private String newsId;
    private String title;
    private String newsContent;
    private String author;
    private String newsDate;

    public News(String newsId, String title, String newsContent, String author, String newsDate) {
        this.newsId = newsId;
        this.title = title;
        this.newsContent = newsContent;
        this.author = author;
        this.newsDate = newsDate;
    }

    public News() {

    }


    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

}

package Service;

import DAL.SqlHelper;
import Entity.News;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsService {

    //查找所有新闻
    public List<News> findNews() throws SQLException {
        String sql = "Select * from news";
        ResultSet rs = SqlHelper.executeQuery(sql);
        List<News> newsList = new ArrayList<News>();

        System.out.println("!!!!!!!!!!!!!!!!!!!");

        while (rs.next()) {
            News news = new News();
            news.setNewsId(rs.getString("NewID"));
            news.setTitle(rs.getString("Title"));
            news.setNewsContent(rs.getString("NewsContent"));
            news.setAuthor(rs.getString("Author"));
            news.setNewsDate(rs.getString("NewsDate"));

            System.out.println("@@@@@@@@@@@@@@");

            newsList.add(news);
        }
        return newsList;
    }

    //删除新闻
    public boolean deleteNews(String newID){
        String sql="delete News with NewID="+newID;
        System.out.println(sql);

        boolean result= SqlHelper.executeUpdate(sql);
        return result;
    }

    //发布新闻
    public boolean addNews(News news) {

        System.out.println(news.getNewsId());
        System.out.println(news.getTitle());
        System.out.println(news.getNewsDate());
        System.out.println(news.getNewsContent());
        System.out.println(news.getNewsId());


        String sql = "INSERT INTO news (NewID, Title, NewsContent, Author, NewsDate) " +
                "VALUES ('"+news.getNewsId()+"', '"+news.getTitle()+"', '"+news.getNewsContent()+"', '"+news.getAuthor()+"', '"+news.getNewsDate()+"');";

        System.out.println(sql);

        boolean result= SqlHelper.executeUpdate(sql);
        return result;
    }

    //修改新闻
    public boolean updateNews(News news){
        System.out.println(news.getNewsContent()+"content");
        String sql="UPDATE news SET " +
                "Title = '"+news.getTitle()+"', NewsContent = '"+news.getNewsContent()+"', Author = '"+news.getAuthor()+"', NewsDate = '"+news.getNewsDate()+"'" +
                "WHERE NewID = '"+news.getNewsId()+"'";
        System.out.println(sql);
        boolean result= SqlHelper.executeUpdate(sql);
        return result;
    }

    //获取当前某条新闻的news对象
    public News GetNews(String newID) throws SQLException {
        String sql="Select * from news where NewID="+newID;
        ResultSet rs;
        rs= SqlHelper.executeQuery(sql);

        News  news=new News();
        if (rs.next()){
            System.out.println("rs");
            news.setNewsId(rs.getString("NewID"));
            news.setTitle(rs.getString("Title"));
            news.setNewsContent(rs.getString("NewsContent"));
            news.setAuthor(rs.getString("Author"));
            news.setNewsDate(rs.getString("NewsDate"));
        }
        return news;
    }

}

<%@ page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%request.setCharacterEncoding("utf-8");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8”>
    <title>新闻页面</title>
    <style type="text/css">
        *{
            padding:0;margin:30px}
        body{
            font:14px/24px "宋体";
            color:#000}
        h2{
            font:normal 22px/35px "黑体";
            color:#000;
            text-align:center;}
        .one{
            font-size:12px;
            text-align:center}
        p{
            text-indent:2em;}
        .blue{
            color:#00C}
        .gray{
            color:#666}
    </style>
</head>
<body>


<h2>${news.title }</h2>
<p class="one gray">时间:<em>${news.newsDate}</em>&nbsp;来源:<em>西南石油大学计科院</em> 作者:<em>${news.author }</em></p>
<hr/>

<p>${news.newsContent }</p>

</body>
</html>

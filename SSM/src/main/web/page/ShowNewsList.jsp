<%@ page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%request.setCharacterEncoding("utf-8");%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>

    <base href="<%=basePath%>">
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8”>
    <title>产品管理</title>
    <link rel="stylesheet" type="text/css" href="page/css/Iframe.css"/>
    <link rel="stylesheet" href="page/utilLib/bootstrap.min.css" type="text/css" media="screen"/>
</head>

<body>
<span class="cp_title">新闻管理</span>
<div class="add_cp">
    <a href="/enterForm">+添加新闻</a>
</div>
<div class="table_con">
    <table>
        <tr class="tb_title">
            <td width="10%">ID</td>
            <td width="30%">标题</td>
            <td width="20%">内容</td>
            <td width="15%">作者</td>
            <td width="10%">时间</td>
            <td width="15%">操作</td>
        </tr>
        <c:forEach var="n" items="${News}">
            <tr>
                <td width="10%" style="overflow: hidden;text-overflow:ellipsis">${n.newID}</td>
                <td width="30%" style="overflow: hidden;text-overflow:ellipsis">${n.title}</td>
                <td width="20%" style="overflow: hidden;text-overflow:ellipsis">${n.newsContent} </td>
                <td width="15%">${n.author}</td>
                <td width="10%">${n.newsDate}</td>
                <td width="15%">
                    <a href="/editNew?newid=${n.newID}" class="bj_btn"
                       style="border: solid 1px #0c89ff;">编辑</a>
                    <a href="/viewNew?newid=${n.newID}" class="sj_btn"
                       style="border:solid 1px #0c89ff; background:#0c89ff; color:#fff">查看</a>
                    <a href="/deleteNew?newid=${n.newID}" class="del_btn"
                       style="border:solid 1px #f12c0b; background:#f12c0b; color:#fff">删除</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>

package com.example.mapper;

import com.example.pojo.News;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsMapper {

    //增加新闻
    @Insert("insert into users.news (NewID,Title, NewsContent, Author, NewsDate)\n" +
            "        values (#{NewID},#{Title},#{NewsContent},#{Author},#{NewsDate});")
    int addNews(News news);

    //删除新闻
    @Delete(" delete from users.news where NewID = #{NewID};")
    int deleteNewsById(@Param("NewID") String newId);

    //修改新闻
    @Update("update users.news\n" +
            "set Title=#{Title},NewsContent=#{NewsContent},Author=#{Author},NewsDate=#{NewsDate}\n" +
            "where NewID = #{NewID};")
    int updateNewsById(News news);

    //查询一条新闻
    @Select("select * from users.news where NewID = #{NewID};")
    News queryNewsById(@Param("NewID") String newId);

    //查询所有新闻
    @Select("select * from users.news;")
    List<News> queryAllNews();
}


package com.example.mapper;

import com.example.pojo.UserInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    //判断当前账号是否正确
    @Select("select * from users_info where UID = #{UID} AND UPWD = #{UPWD}")
    UserInfo isUserExist(UserInfo userInfo);
}

package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getUPWD() {
        return UPWD;
    }

    public void setUPWD(String UPWD) {
        this.UPWD = UPWD;
    }

    String UID="";
    String UPWD="";

}

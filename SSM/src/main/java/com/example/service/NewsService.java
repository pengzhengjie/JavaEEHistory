package com.example.service;

import com.example.pojo.News;

import java.util.List;

public interface NewsService {
    //增加新闻
    boolean addNews(News news);

    //删除新闻
    boolean deleteNewsById(String newId);

    //修改新闻
    boolean updateNewsById(News news);

    //查询一条新闻
    News queryNewsById(String newId);

    //查询所有新闻
    List<News> queryAllNews();
}

package com.example.service.impl;

import com.example.mapper.NewsMapper;
import com.example.pojo.News;
import com.example.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("NewsService")
public class NewsServiceImpl implements NewsService {
    //service调dao层  组合mapper
    @Autowired
    private NewsMapper newsMapper;

    public void setNewsMapper(NewsMapper newsMapper) {
        //可增加其他代码
        this.newsMapper = newsMapper;
    }

    public boolean addNews(News news) {

        int rows = newsMapper.addNews(news);
        if (rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteNewsById(String newId) {
        int rows = newsMapper.deleteNewsById(newId);
        if (rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updateNewsById(News news) {
        int rows = newsMapper.updateNewsById(news);
        if (rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public News queryNewsById(String newId) {
        return newsMapper.queryNewsById(newId);
    }

    public List<News> queryAllNews() {
        return newsMapper.queryAllNews();
    }
}

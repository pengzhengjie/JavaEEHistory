package com.example.service.impl;

import com.example.mapper.UserMapper;
import com.example.pojo.UserInfo;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("UserService")
public class UserServiceImpl implements UserService {
    //service调dao层  组合mapper
    @Autowired
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    //账号结果是否正确
    public boolean isUserExist(UserInfo userInfo) {
        UserInfo quertRes = userMapper.isUserExist(userInfo);
        if (quertRes == null) {
            return false;
        }

        if (quertRes.getUID() != null && quertRes.getUPWD() != null) {
            return true;
        } else {
            return false;
        }
    }
}

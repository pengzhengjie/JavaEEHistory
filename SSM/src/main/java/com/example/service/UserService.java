package com.example.service;

import com.example.pojo.UserInfo;

public interface UserService {
    //判断当前账号是否正确
    boolean isUserExist(UserInfo userInfo);
}

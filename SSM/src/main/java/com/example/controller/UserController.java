package com.example.controller;

import com.example.pojo.UserInfo;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UserInfo user = new UserInfo();
        user.setUID(username);
        user.setUPWD(password);

        boolean b = userService.isUserExist(user);
        if (b) {
            response.sendRedirect("page/index.html");
        } else {
            response.sendRedirect("index.jsp");
        }
    }
}

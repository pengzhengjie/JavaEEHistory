package com.example.controller;

import com.example.pojo.News;
import com.example.pojo.UserInfo;
import com.example.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    @RequestMapping("/showNews")
    public void showNews(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        try {
            List<News> newsList = newsService.queryAllNews();
            for (News item : newsList) {
                System.out.println(item.getNewID().toString());
            }

            request.setAttribute("News", newsList);
            request.getRequestDispatcher("/page/ShowNewsList.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/enterForm")
    public void enterForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        request.getRequestDispatcher("AddNews.jsp").forward(request, response);
    }


    @RequestMapping("/addNew")
    public void addNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        News news = new News();

        news.setNewID(request.getParameter("newsID"));
        news.setAuthor(request.getParameter("newsAuthor"));
        news.setTitle(request.getParameter("newsTitle"));
        news.setNewsDate(request.getParameter("newsDate"));
        news.setNewsContent(request.getParameter("editorValue"));


        newsService.addNews(news);

        request.getRequestDispatcher("showNews").forward(request, response);
    }

    @RequestMapping("/editNew")
    public void editNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        String newsID = request.getParameter("newid");

        try {
            News news = newsService.queryNewsById(newsID);
            request.setAttribute("news", news);

            request.getRequestDispatcher("EditNews.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/saveNew")
    public void saveNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        News news = new News();

        news.setNewID(request.getParameter("newsID"));
        news.setAuthor(request.getParameter("newsAuthor"));
        news.setTitle(request.getParameter("newsTitle"));
        news.setNewsDate(request.getParameter("newsDate"));
        news.setNewsContent(request.getParameter("editorValue"));

        newsService.updateNewsById(news);
        request.getRequestDispatcher("showNews").forward(request, response);
    }


    @RequestMapping("/deleteNew")
    public void deleteNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String newsID = request.getParameter("newid");

        newsService.deleteNewsById(newsID);
        request.getRequestDispatcher("showNews").forward(request, response);
    }

    @RequestMapping("/viewNew")
    public void viewNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        String newsID = request.getParameter("newid");
        System.out.println(newsID);

        News news = null;
        news = newsService.queryNewsById(newsID);

        System.out.println(news.getNewID());
        System.out.println(news.getNewsContent());
        System.out.println(news.getNewsDate());
        System.out.println(news.getAuthor());

        request.setAttribute("news", news);
        //转发到content.jsp
        request.getRequestDispatcher("NewsContent.jsp").forward(request, response);
    }

    @RequestMapping("/home")
    public void home(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");


        List<News> newsList = newsService.queryAllNews();
        try {

            for (News item : newsList) {
                System.out.println(item.getNewID().toString());
                request.setAttribute("News", newsList);
                request.getRequestDispatcher("home.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
